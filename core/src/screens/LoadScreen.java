package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import engine.Loader;
import engine.SpecialForces;
import view.Image;
import view.gui.ProgressBar;
import view.gui.ProgressBarStyle;

public class LoadScreen implements Screen{
	
	private Loader loader;
	private Stage stage;
	private Image background;
	private ProgressBar loadBar;
	
	public LoadScreen(Loader loader, SpriteBatch batch) {
		this.loader = loader;
		OrthographicCamera camera = new OrthographicCamera(SpecialForces.WIDTH, SpecialForces.HEIGHT);
		stage = new Stage(new StretchViewport(SpecialForces.WIDTH, SpecialForces.HEIGHT, camera), batch);
		background = new Image(loader.getBackground("load"));
		loadBar = new ProgressBar(loader, ProgressBarStyle.LOAD);
		loadBar.setPosition(SpecialForces.WIDTH / 2 - loadBar.getWidth() / 2, 150);
		
		
		stage.addActor(background);
		stage.addActor(loadBar);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render(float delta) {
		if (loader.getAssetManager().update()){
			SpecialForces.getInstance().loadingDone();
			done();
		}
		loadBar.setProgress(loader.getAssetManager().getProgress());
		stage.act(delta);
		stage.draw();
	}
	
	private void done(){
		SpecialForces.getInstance().screenManager().show(ScreenType.MENU);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().setScreenSize(width, height);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
}
