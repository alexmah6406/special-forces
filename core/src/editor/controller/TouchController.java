package editor.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

import stages.Editor;

public class TouchController implements GestureListener, InputProcessor{
	private Editor editor;
	private float downX, downY, x, y, oldX, oldY;
	private float zoomAmount, oldZoomAmount;

	public TouchController(Editor editor) {
		this.editor = editor;
	}
	
	public void update() {
		editor.zoomBy(getZoom());
	}
	
	private float getZoom(){
		final float zoom = ((zoomAmount - oldZoomAmount) * 0.0025f);
		oldZoomAmount = zoomAmount;
		return zoom;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		if (pointer > 0)
			return false;
		downX = x;
		downY = y;
		oldX = 0;
		oldY = 0;
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		touchDragged(Gdx.input.getX(), Gdx.input.getY(), 0);
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		oldZoomAmount = zoomAmount;
		zoomAmount = initialDistance - distance;
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void pinchStop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (pointer > 0)
			return false;
		x = screenX - downX;
		y = screenY - downY;
		editor.moveCamera(-(x - oldX), y - oldY);
		oldX = x;
		oldY = y;
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
