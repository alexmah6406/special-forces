package editor;

import com.badlogic.gdx.utils.GdxRuntimeException;

import engine.Loader;
import engine.SpecialForces;
import engine.Style;
import engine.utils.Maths;
import stages.Editor;
import world.map.MapBuilder;

public class Manager {
	
	private Editor editor;
	private Loader loader;
	
	public Manager(Editor editor, Loader loader){
		this.editor = editor;
		this.loader = loader;
		newMap(10, 10, 0);
		editor.getOrtCam().position.set((editor.map().getMapWidth() * Style.TILE_SIZE)/2f, (editor.map().getMapHeight() * Style.TILE_SIZE)/2f, 0);
	}
	
	public void testMap(){
		if (!editor.objects().isPlayerCreated()){
			editor.gui().showMessage("Player is not created");
			return;
		}
		if (!editor.objects().isPlayerSingular()){
			editor.gui().showMessage("Map could have only\none player");
			return;
		}
		String map = mapToString();
		SpecialForces.getInstance().screenManager().startTest(map, SpecialForces.getInstance().playerData());
	}
	
	public void save(int index){
		loader.saveToFile("bin/maps/editor".concat(String.valueOf(index)).concat(".txt"), mapToString());
	}
	
	public void open(int index){
		try{
			String map = loader.loadFromFile("bin/maps/editor".concat(String.valueOf(index)).concat(".txt"));
			clear();
			editor.map().loadFromString(map);
			editor.map().build();
		}catch(GdxRuntimeException e){
			editor.gui().showMessage("This slot is empty");
		}
	}
	
	public void newMap(int width, int height, int tileIndex) {
		StringBuilder sb = new StringBuilder();
		sb.append("#map\n");
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++)
				sb.append(MapBuilder.convertToFile((char) tileIndex));
			sb.append('\n');
		}
		sb.append("#objects\n#end");
		openFromString(sb.toString());
	}
	
	public void clear(){
		editor.objects().free();
		editor.map().freeMap();
		editor.gui().updateProperties(null);
	}
	
	public void show(){
		Maths.setWorld(editor);
	}
	
	public void hide(){
		
	}
	
	private String mapToString(){
		StringBuilder sb = new StringBuilder();
		sb.append("#map\n");
		editor.map().generate(sb);
		sb.append("#objects\n");
		editor.objects().generate(sb);
		sb.append("#end");
		return sb.toString();
	}
	
	private void openFromString(String map) {
		clear();
		editor.map().loadFromString(map);
		editor.map().build();
	}
}
