package view.joystick;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;

import engine.Loader;
import engine.utils.Maths;
import view.Button;

public class Stick extends Button{
	private float px, py, dx, dy, cof, len;
	private final float radius = 40;
	private Vector2 vect;
	
	private JoystickListener listener;

	public Stick(Loader loader) {
		super(loader.getHud("btnMove"), 0, 0);
		setSize(150, 150);
		setOrigin(Align.center);
		vect = Maths.getTmpVector();
		addListener(new StickListener(this));
	}
	
	public void dragDown(float x, float y){
		px = getX();
		py = getY();
		dx = x;
		dy = y;
		if (listener != null)
			listener.dragDown(x, y);
	}
	
	public void dragged(float x, float y){
		len = vect.set(x - px - dx - getParent().getX(), y - py - dy - getParent().getY()).len();
		vect.nor();
		cof = 1f / radius * len;
		if (cof > 1)
			cof = 1;
		setPosition(px + vect.x * radius * cof, py + vect.y * radius * cof);
		if (listener != null)
			listener.dragged(vect.x, vect.y, cof);
	}
	
	public void dragUp(){
		setPosition(px, py);
		if (listener != null)
			listener.dragUp();
	}
	
	public void setListener(JoystickListener listener){
		this.listener = listener;
	}

}