package view.joystick;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class StickListener extends InputListener{
	private Stick stick;
	
	public StickListener(Stick stick){
		this.stick = stick;
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		this.stick.dragDown(x, y);
		return true;
	}
	
	@Override
	public void touchDragged(InputEvent event, float x, float y, int pointer) {
		stick.dragged(Gdx.input.getX(pointer), Gdx.graphics.getHeight() - Gdx.input.getY(pointer));
	}
	
	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		this.stick.dragUp();
	}
}
