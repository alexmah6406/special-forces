package view.menu;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import engine.Loader;
import engine.Style;
import view.Actor;
import view.Font;

public class Menu extends Actor{
	
	private BitmapFont font;
	
	private HashMap<String, String[][]> pages;
	private String[][] selPage;
	
	private int index, i, halign;
	
	public Menu(Loader loader, Font fontType, int halign, float width, float height){
		pages = new HashMap<String, String[][]>();
		font = loader.getFont(fontType);
		this.halign = halign;
		setSize(width, height);
	}
	
	public void addPage(String name, String[][] page){
		pages.put(name, page);
	}
	
	public void showPage(String name){
		selPage = pages.get(name);
		index = 0;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		for (i = 0; i < selPage.length; i++){
			if (index == i)
				font.setColor(0.1f, 0.8f, 0.1f, 1);
			font.draw(batch, selPage[i][0], getX(), getHeight() + getY() - (font.getCapHeight() + Style.MENU_ITEM_OFFSET) * i, getWidth(), halign, false);
			font.setColor(1, 1, 1, 1);
		}
	}
	
	public void next(){
		if (selPage == null)
			return;
		if (index < selPage.length-1)
			index++;
	}
	
	public void prev(){
		if (index > 0)
			index--;
	}
	
	public void enter(){
		if (selPage == null)
			return;
		String action = selPage[index][1];
		if (action != null)
			showPage(action);
	}
}
