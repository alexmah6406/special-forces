package view.menu;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class MenuController extends InputListener{
	private Menu menu;
	public MenuController(Menu menu){
		this.menu = menu;
	}
	
	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		if (keycode == Keys.DOWN)
			menu.next();
		if (keycode == Keys.UP)
			menu.prev();
		if (keycode == Keys.ENTER)
			menu.enter();
		return true;
	}
}