package view;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;

public class Button extends Actor{
	
	private TextureRegion texture;
	
	public Button(TextureRegion texture, float x, float y){
		this.texture = texture;
		setSize(texture.getRegionWidth(), texture.getRegionHeight());
		setOrigin(Align.center);
		setPosition(x, y);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(texture, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
	}
}
